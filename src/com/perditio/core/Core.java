/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.perditio.core;

import com.perditio.client.Perditio;
import com.perditio.client.imperatum.Imperatum;
import com.perditio.client.telum.Telum;

/**
 *
 * @author simplyianm
 */
public class Core extends Telum {
    private CoreCommands coreCommands;
    
    @Override
    public void onEnable() {
        coreCommands = new CoreCommands(perditio);
        coreCommands.initialize();
        coreCommands.registerAll();
    }

    @Override
    public void onDisable() {
        //
    }

    @Override
    public String getName() {
        return "Core";
    }

    @Override
    public String getDescription() {
        return "The core telum within the client.";
    }
}
