/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.perditio.core.aura;

import com.perditio.core.commands.BaseCommand;
import com.perditio.client.Perditio;
import com.perditio.client.imperatum.Imperatum;
import com.perditio.client.telum.Telum;
import com.perditio.client.util.ChatColor;
import java.util.List;
import net.minecraft.src.Block;
import net.minecraft.src.Entity;
import net.minecraft.src.EntityLiving;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.Packet18Animation;
import net.minecraft.src.Packet7UseEntity;

/**
 *
 * @author simplyianm
 */
public class AuraAntimob extends AbstractAura {
    private boolean toggled;

    @Override
    public String getName() {
        return "Antimob";
    }

    @Override
    public String getDescription() {
        return "Kills nearby mobs.";
    }

    @Override
    public void updateAura() {
        List<Entity> localEntities = perditio.getMinecraft().theWorld.loadedEntityList;
        for (Entity entity: localEntities) {
            if (!(entity instanceof EntityLiving)) continue;
            if (entity instanceof EntityPlayer) continue;
            double xDist1 = perditio.getMinecraft().thePlayer.posX - entity.posX;
            double yDist1 = perditio.getMinecraft().thePlayer.posY - entity.posY;
            double zDist1 = perditio.getMinecraft().thePlayer.posZ - entity.posZ;
            double pythagorean = xDist1 * xDist1 + yDist1 * yDist1 + zDist1 * zDist1;
            if (pythagorean <= 25) { //Check for distance of 5 away
                Packet7UseEntity packet7useentity = new Packet7UseEntity(((EntityPlayer) (perditio.getMinecraft().thePlayer)).entityId, entity.entityId, 2);
                Packet18Animation packet18animation = new Packet18Animation(perditio.getMinecraft().thePlayer, 1);
                perditio.getMinecraft().getSendQueue().addToSendQueue(packet7useentity);
                perditio.getMinecraft().getSendQueue().addToSendQueue(packet18animation);
                perditio.getMinecraft().thePlayer.useCurrentItemOnEntity(entity);
                try {
                    Thread.sleep(100L);
                } catch (InterruptedException e) {
                }
            }
        }
    }
}
