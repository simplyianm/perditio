/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.perditio.core.aura;

import com.perditio.core.commands.BaseCommand;
import com.perditio.client.Perditio;
import com.perditio.client.imperatum.Imperatum;
import com.perditio.client.telum.Telum;
import com.perditio.client.util.ChatColor;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.minecraft.src.Block;
import net.minecraft.src.Packet14BlockDig;

/**
 *
 * @author simplyianm
 */
public class AuraCreativeNuke extends AbstractAura {
    private boolean toggled;

    @Override
    public String getName() {
        return "Nuker";
    }

    @Override
    public String getDescription() {
        return "Breaks all blocks";
    }

    @Override
    public void updateAura() {
        int playerX = (int) perditio.getMinecraft().thePlayer.posX;
        int playerY = (int) perditio.getMinecraft().thePlayer.posY;
        int playerZ = (int) perditio.getMinecraft().thePlayer.posZ;
        int minX = playerX - 5;
        int minY = 5;
        int minZ = playerZ - 5;
        int maxX = playerX + 5;
        int maxY = 127;
        int maxZ = playerZ + 5;
        for (int curBlockX = minX; curBlockX < maxX; curBlockX++) {
            int xDist = curBlockX - playerX;
            for (int curBlockY = minY; curBlockY < maxY; curBlockY++) {
                int yDist = curBlockY - playerY;
                for (int curBlockZ = minZ; curBlockZ < maxZ; curBlockZ++) {
                    int zDist = curBlockZ - playerZ;
                    int theCurBlockID = perditio.getMinecraft().theWorld.getBlockId(curBlockX, curBlockY, curBlockZ);
                    Block blok = theCurBlockID <= 0 ? null : Block.blocksList[theCurBlockID];
                    if(blok != null) {//Check if block is valid to send to the instant break queue
                        perditio.getMinecraft().getSendQueue().addToSendQueue(new Packet14BlockDig(0, curBlockX, curBlockY, curBlockZ, 1));
                    }
                }
            }
        }
        try {
            Thread.sleep(50L);
        } catch (InterruptedException ex) {
            Logger.getLogger(AuraCreativeNuke.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
