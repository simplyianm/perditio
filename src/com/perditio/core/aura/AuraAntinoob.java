/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.perditio.core.aura;

import com.perditio.core.commands.BaseCommand;
import com.perditio.client.Perditio;
import com.perditio.client.imperatum.Imperatum;
import com.perditio.client.telum.Telum;
import com.perditio.client.util.ChatColor;
import java.util.List;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.Packet18Animation;
import net.minecraft.src.Packet7UseEntity;

/**
 *
 * @author simplyianm
 */
public class AuraAntinoob extends AbstractAura {
    @Override
    public String getName() {
        return "Anti-Noob";
    }

    @Override
    public String getDescription() {
        return "Kills nearby players.";
    }

    @Override
    public void updateAura() {
        List<EntityPlayer> localPlayers = perditio.getMinecraft().theWorld.playerEntities;
        for (EntityPlayer player : localPlayers) {
            if (!(player instanceof EntityPlayer)) {
                double xDist1 = perditio.getMinecraft().thePlayer.posX - player.posX;
                double yDist1 = perditio.getMinecraft().thePlayer.posY - player.posY;
                double zDist1 = perditio.getMinecraft().thePlayer.posZ - player.posZ;
                double pythagorean = xDist1 * xDist1 + yDist1 * yDist1 + zDist1 * zDist1;
                if (pythagorean <= 25) { //Check for distance of 5 away
                    Packet7UseEntity packet7useentity = new Packet7UseEntity(((EntityPlayer) (perditio.getMinecraft().thePlayer)).entityId, player.entityId, 2);
                    Packet18Animation packet18animation = new Packet18Animation(perditio.getMinecraft().thePlayer, 1);
                    perditio.getMinecraft().getSendQueue().addToSendQueue(packet7useentity);
                    perditio.getMinecraft().thePlayer.useCurrentItemOnEntity(player);
                    perditio.getMinecraft().thePlayer.useCurrentItemOnEntity(player);
                    perditio.getMinecraft().getSendQueue().addToSendQueue(packet18animation);
                    try {
                        Thread.sleep(35L);
                    } catch (InterruptedException e) {
                    }
                }
            }
        }
    }
}
