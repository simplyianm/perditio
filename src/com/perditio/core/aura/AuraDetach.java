/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.perditio.core.aura;

import java.util.logging.Level;
import java.util.logging.Logger;
import net.minecraft.src.Block;
import net.minecraft.src.Packet14BlockDig;

/**
 *
 * @author simplyianm
 */
public class AuraDetach extends AbstractAura {
    @Override
    public String getName() {
        return "Detach";
    }

    @Override
    public String getDescription() {
        return "Detaches torches.";
    }

    @Override
    public void updateAura() {int playerX = (int) perditio.getMinecraft().thePlayer.posX;
        int playerY = (int) perditio.getMinecraft().thePlayer.posY;
        int playerZ = (int) perditio.getMinecraft().thePlayer.posZ;
        int minX = playerX - 5;
        int minY = playerY - 5;
        int minZ = playerZ - 5;
        int maxX = playerX + 5;
        int maxY = playerY + 5;
        int maxZ = playerZ + 5;
        for (int curBlockX = minX; curBlockX < maxX; curBlockX++) {
            int xDist = curBlockX - playerX;
            for (int curBlockY = minY; curBlockY < maxY; curBlockY++) {
                int yDist = curBlockY - playerY;
                for (int curBlockZ = minZ; curBlockZ < maxZ; curBlockZ++) {
                    int zDist = curBlockZ - playerZ;
                    if (xDist * xDist + yDist * yDist + zDist * zDist <= 25) { //If witin the sphere
                        int theCurBlockID = perditio.getMinecraft().theWorld.getBlockId(curBlockX, curBlockY, curBlockZ);
                        Block blok = theCurBlockID <= 0 ? null : Block.blocksList[theCurBlockID];
                        if(blok != null && (blok == Block.torchWood || blok == Block.torchRedstoneIdle || blok == Block.torchRedstoneActive || blok == Block.redstoneWire || blok == Block.redstoneRepeaterIdle || blok == Block.redstoneRepeaterActive || blok == Block.plantYellow || blok == Block.plantRed || blok == Block.mushroomRed || blok == Block.mushroomBrown || blok == Block.crops || blok == Block.fire || blok == Block.tnt || blok == Block.sapling || blok == Block.deadBush || blok == Block.tallGrass || blok == Block.reed)) {//Check if block is valid to send to the instant break queue
                            perditio.getMinecraft().getSendQueue().addToSendQueue(new Packet14BlockDig(0, curBlockX, curBlockY, curBlockZ, 1));
                        }
                    }
                }
            }
        }
        try {
            Thread.sleep(30L);
        } catch (InterruptedException ex) {
            Logger.getLogger(AuraDetach.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
