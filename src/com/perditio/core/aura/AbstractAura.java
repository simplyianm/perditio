/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.perditio.core.aura;

import com.perditio.client.Perditio;
import com.perditio.client.util.ChatColor;

/**
 *
 * @author simplyianm
 */
public abstract class AbstractAura {
    protected boolean toggled;
    protected Perditio perditio;
    
    public void toggle() {
        toggled = !toggled;
        
        if (toggled) {
            AuraUpdater auraUpdater = new AuraUpdater();
            Thread auraThread = new Thread(auraUpdater, "perditio-core-aura-" + this.getName().toLowerCase());
            auraThread.start();
        }
        
        String toggleMessage = toggled ? "enabled" : "disabled";
        perditio.getMinecraft().thePlayer.addChatMessage(ChatColor.BLUE + this.getName() + " " + toggleMessage + ".");
    }
    
    public abstract String getName();
    
    public abstract String getDescription();
    
    public abstract void updateAura();
    
    private class AuraUpdater implements Runnable {

        @Override
        public void run() {
            while (toggled) {
                updateAura();
            }
        }
        
    }
}
