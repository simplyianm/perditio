/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.perditio.core;

import com.perditio.client.Perditio;
import com.perditio.core.commands.BaseCommand;
import com.perditio.core.commands.CommandBye;
import com.perditio.core.commands.CommandClimb;
import com.perditio.core.commands.CommandDos;
import com.perditio.core.commands.CommandDrop;
import com.perditio.core.commands.CommandFly;
import com.perditio.core.commands.CommandHoverjump;
import com.perditio.core.commands.CommandPrognuke;
import com.perditio.core.commands.CommandShutdown;
import com.perditio.core.commands.CommandSpam;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author simplyianm
 */
public class CoreCommands {
    private List<BaseCommand> commands = new ArrayList<BaseCommand>();
    private Perditio perditio;
    
    public CoreCommands(Perditio perditio) {
        this.perditio = perditio;
    }
    
    public void initialize() {
        BaseCommand commandBye = new CommandBye();
        BaseCommand commandClimb = new CommandClimb();
        BaseCommand commandDos = new CommandDos();
        BaseCommand commandSpammer = new CommandSpam();
        BaseCommand commandDrop = new CommandDrop();
        BaseCommand commandFly = new CommandFly();
        BaseCommand commandHoverjump = new CommandHoverjump();
        BaseCommand commandShutdown = new CommandShutdown();
        BaseCommand commandPrognuke = new CommandPrognuke();
        
        commands.add(commandBye);
        commands.add(commandClimb);
        commands.add(commandDos);
        commands.add(commandSpammer);
        commands.add(commandDrop);
        commands.add(commandFly);
        commands.add(commandHoverjump);
        commands.add(commandShutdown);
        commands.add(commandPrognuke);
    }
    
    public void registerAll() {
        for (BaseCommand command : commands) {
            command.initialize(perditio);
            perditio.getImperatumHandler().registerExecutor(command.getLabel().toLowerCase(), command);
        }
    }
}
