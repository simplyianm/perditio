/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.perditio.core.commands;

import com.perditio.client.Perditio;
import com.perditio.client.imperatum.Imperatum;
import com.perditio.client.telum.Telum;
import com.perditio.client.util.ChatColor;
import net.minecraft.client.Minecraft;
import org.lwjgl.input.Keyboard;

/**
 *
 * @author simplyianm
 */
public class CommandFly extends BaseCommand {
    private boolean toggled;

    @Override
    public String getLabel() {
        return "fly";
    }

    @Override
    public String getDescription() {
        return "Makes the player fly.";
    }

    @Override
    public void onImperatum(Imperatum imperatum) {
        toggled = !toggled;

        if (toggled) {
            Flier flier = new Flier();
            Thread flyThread = new Thread(flier, "Perditio_BaseCommand_Fly");
            flyThread.start();
        }

        String toggleMessage = toggled ? "enabled" : "disabled";
        perditio.getMinecraft().thePlayer.addChatMessage(ChatColor.BLUE + this.getLabel() + " " + toggleMessage + ".");
    }
    
    private class Flier implements Runnable {

        @Override
        public void run() {
            while (toggled) {
                Minecraft minecraft = perditio.getMinecraft();
                minecraft.thePlayer.onGround = false;
                minecraft.thePlayer.motionX = 0.0D;
                minecraft.thePlayer.motionY = 0.0D;
                minecraft.thePlayer.motionZ = 0.0D;


                double d1 = minecraft.thePlayer.rotationPitch + 90F;
                double d2 = minecraft.thePlayer.rotationYaw + 90F;
                boolean moveForward = Keyboard.isKeyDown(Keyboard.KEY_W) && minecraft.inGameHasFocus;
                boolean moveBackward = Keyboard.isKeyDown(Keyboard.KEY_S) && minecraft.inGameHasFocus;
                boolean strafeLeft = Keyboard.isKeyDown(Keyboard.KEY_A) && minecraft.inGameHasFocus;
                boolean strafeRight = Keyboard.isKeyDown(Keyboard.KEY_D) && minecraft.inGameHasFocus;
                if(moveForward) {
                    if (strafeLeft) {
                        d2 -= 10D;
                    } else if (strafeRight) {
                        d2 += 10D;
                    }
                } else if(moveBackward) {
                    d2 += 40D;
                    if(strafeLeft) {
                        d2 += 10D;
                    } else if(strafeRight) {
                        d2 -= 10D;
                    }
                } else if(strafeLeft) {
                    d2 -= 20D;
                } else if(strafeRight) {
                    d2 += 20D;
                }

                //Move
                if(moveForward || strafeLeft || moveBackward || strafeRight) {
                    minecraft.thePlayer.motionX = Math.cos(Math.toRadians(d2));
                    minecraft.thePlayer.motionZ = Math.sin(Math.toRadians(d2));
                }

                //Go up
                if(Keyboard.isKeyDown(Keyboard.KEY_SPACE) && minecraft.inGameHasFocus) {
                    minecraft.thePlayer.motionY += 3D;
                //Go down
                } else if(Keyboard.isKeyDown(Keyboard.KEY_Z) && minecraft.inGameHasFocus) {
                    minecraft.thePlayer.motionY -= 3D;
                }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                

                if (Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)) {
                    minecraft.thePlayer.motionX /= 4D;
                    minecraft.thePlayer.motionY /= 4D;
                    minecraft.thePlayer.motionZ /= 4D;
                }
            }
        }
        
    }
}
