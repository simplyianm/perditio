/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.perditio.core.commands;

import com.perditio.client.Perditio;
import com.perditio.client.imperatum.Imperatum;
import com.perditio.client.telum.Telum;
import com.perditio.client.util.ChatColor;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.minecraft.src.Block;
import net.minecraft.src.Packet14BlockDig;

/**
 *
 * @author simplyianm
 */
public class CommandPrognuke extends BaseCommand {
    private boolean toggled;

    @Override
    public String getLabel() {
        return "prognuke";
    }

    @Override
    public String getDescription() {
        return "Progressively flattens a map.";
    }

    @Override
    public void onImperatum(Imperatum imperatum) {
        int xLength = 50;
        int zLength = 50;
        int yMin = 50;
        int yMax = 70;

        String[] args = imperatum.getArgs();
        if (args.length > 0) {
            xLength = Integer.valueOf(args[0]);
            zLength = Integer.valueOf(args[0]);
        }

        toggled = !toggled;

        if (toggled) {
            Thread prognukeThread = new Thread(new Prognuker(xLength, zLength, yMin, yMax), "TelumPrognuke");
            prognukeThread.start();
        }

        String toggleMessage = toggled ? "enabled" : "disabled";
        perditio.getMinecraft().thePlayer.addChatMessage(ChatColor.RED + this.getLabel() + " " + toggleMessage + ".");
    }
    
    private class Prognuker implements Runnable {
        private int xLength;
        private int zLength;
        private int yMin;
        private int yMax;

        public Prognuker(int xLength, int zLength, int yMin, int yMax) {
            this.xLength = xLength;
            this.zLength = zLength;
            this.yMin = yMin;
            this.yMax = yMax;
        }

        
        /**
         * Run
         */
        @Override
        public void run() {
            int playerX = (int) perditio.getMinecraft().thePlayer.posX;
            int playerZ = (int) perditio.getMinecraft().thePlayer.posZ;
            
            int minX = playerX - (xLength / 2);
            int minZ = playerZ - (zLength / 2);
            
            int maxX = playerX + (xLength / 2);
            int maxZ = playerZ + (zLength / 2);
            
            for (int curBlockX = minX; curBlockX < maxX; curBlockX++) {
                
                //Column
                for (int curBlockY = yMin; curBlockY < yMax; curBlockY++) {
                    
                    //Row
                    for (int curBlockZ = minZ; curBlockZ < maxZ; curBlockZ++) {
                        
                        int theCurBlockID = perditio.getMinecraft().theWorld.getBlockId(curBlockX, curBlockY, curBlockZ);
                        Block blok = theCurBlockID <= 0 ? null : Block.blocksList[theCurBlockID];
                        if(blok != null) {//Check if block is valid to send to the instant break queue
                            perditio.getMinecraft().getSendQueue().addToSendQueue(new Packet14BlockDig(0, curBlockX, curBlockY, curBlockZ, 1));
                        }
                        
                        try {
                            Thread.sleep(1L);
                        } catch (InterruptedException ex) {
                            Logger.getLogger(CommandPrognuke.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        
                    }
                    
                } //End columns
                try {
                    Thread.sleep(1000L);
                } catch (InterruptedException ex) {
                    Logger.getLogger(CommandPrognuke.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            }
        }
    }
}
