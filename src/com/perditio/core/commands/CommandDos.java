/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.perditio.core.commands;

import com.perditio.client.Perditio;
import com.perditio.client.imperatum.Imperatum;
import com.perditio.client.telum.Telum;
import com.perditio.client.util.ChatColor;
import net.minecraft.src.Packet3Chat;

/**
 *
 * @author simplyianm
 */
public class CommandDos extends BaseCommand {

    @Override
    public String getLabel() {
        return "dos";
    }

    @Override
    public String getDescription() {
        return "Sends a bunch of messages that force the server to kick the originato of the messages.";
    }

    @Override
    public void onImperatum(Imperatum imperatum) {
        Doser doser = new Doser(imperatum);
        Thread dosThread = new Thread(doser, "TelumSpam");
        dosThread.start();
    }
    
    private class Doser implements Runnable {
        private Imperatum imperatum;

        public Doser(Imperatum imperatum) {
            this.imperatum = imperatum;
        }
        
        /**
         * Run
         */
        @Override
        public void run() {
            String amountStr = "100";
            if (imperatum.getArgs().length > 1) amountStr = imperatum.getArgs()[1];
            int amount;
            try {
                amount = Integer.parseInt(amountStr);
            } catch (NumberFormatException ex) {
                amount = 5;
            }
            for (int m = 0; m < amount; m++) {
                perditio.sendPacket(new Packet3Chat(ChatColor.YELLOW + "Derp"));
            }
        }
    }
}
