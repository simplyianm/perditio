/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.perditio.core.commands;

import com.perditio.client.imperatum.Imperatum;
import com.perditio.client.util.ChatColor;
import org.lwjgl.input.Keyboard;

/**
 *
 * @author simplyianm
 */
public class CommandHoverjump extends BaseCommand {
    private boolean toggled;

    @Override
    public String getLabel() {
        return "hoverjump";
    }

    @Override
    public String getDescription() {
        return "Makes the player jump super high.";
    }

    @Override
    public void onImperatum(Imperatum imperatum) {
        toggled = !toggled;

        if (toggled) {
            Hoverjumper hoverjumper = new Hoverjumper();
            Thread hoverjumpThread = new Thread(hoverjumper, "FlyThread");
            hoverjumpThread.start();
        }

        String toggleMessage = toggled ? "enabled" : "disabled";
        perditio.getMinecraft().thePlayer.addChatMessage(ChatColor.BLUE + this.getLabel() + " " + toggleMessage + ".");
    }
    
    private class Hoverjumper implements Runnable {

        @Override
        public void run() {
            while (toggled) {
                if (Keyboard.isKeyDown(Keyboard.KEY_SPACE)) {
                    if (perditio.getMinecraft().thePlayer.onGround == true) perditio.getMinecraft().thePlayer.setVelocity(0, 3, 0);
                } else {
                    perditio.getMinecraft().thePlayer.onGround = true;
                }
            }
        }
        
    }
}
