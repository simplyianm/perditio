/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.perditio.core.commands;

import com.perditio.client.imperatum.Imperatum;

/**
 *
 * @author simplyianm
 */
public class CommandShutdown extends BaseCommand {

    @Override
    public String getLabel() {
        return "shutdown";
    }

    @Override
    public String getDescription() {
        return "A panic button.";
    }

    @Override
    public void onImperatum(Imperatum imperatum) {
        perditio.getMinecraft().shutdown();
    }
    
}
