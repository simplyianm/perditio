/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.perditio.core.commands;

import com.perditio.client.Perditio;
import com.perditio.client.imperatum.ImperatumExecutor;

/**
 *
 * @author simplyianm
 */
public abstract class BaseCommand implements ImperatumExecutor {
    protected Perditio perditio;
    
    public void initialize(Perditio perditio) {
        this.perditio = perditio;
    }
    
    public abstract String getLabel();
    
    public abstract String getDescription();
}
