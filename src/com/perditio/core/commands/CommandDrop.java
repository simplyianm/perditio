/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.perditio.core.commands;

import com.perditio.client.Perditio;
import com.perditio.client.imperatum.Imperatum;
import com.perditio.client.telum.Telum;
import com.perditio.client.util.ChatColor;
import net.minecraft.src.InventoryPlayer;

/**
 *
 * @author simplyianm
 */
public class CommandDrop extends BaseCommand {
    private boolean toggled = false;

    @Override
    public String getLabel() {
        return "drop";
    }

    @Override
    public String getDescription() {
        return "Drops items quickly to induce lag.";
    }

    @Override
    public void onImperatum(Imperatum imperatum) {
        Dropper dropper = new Dropper();
        Thread dropperThread = new Thread(dropper, "Perditio_BaseCommand_dropper");
        dropperThread.start();
    }
    
    
    private class Dropper implements Runnable {
        @Override
        public void run() {
            InventoryPlayer inv = perditio.getMinecraft().thePlayer.inventory;
            inv.changeCurrentItem(1);
            for (int slotID = 1; slotID <= 10; slotID++) {
                //TFHack.minecraft.thePlayer.addChatMessage("Slot id " + slotID);
                //ItemStack currentStack = minecraft.thePlayer.inventory.mainInventory[26 + slotID];
                for (int i = 0; i < 64; i++) { //Drops one stack
                    perditio.getMinecraft().thePlayer.dropCurrentItem();

                }
                if (slotID != 9) inv.changeCurrentItem(slotID + 1);
                try {
                    Thread.sleep(115L);                
                } catch (InterruptedException e) {
                        //
                }
            }
        }
    }
}
