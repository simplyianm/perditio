/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.perditio.core.commands;

import com.perditio.client.Perditio;
import com.perditio.client.imperatum.Imperatum;
import com.perditio.client.telum.Telum;
import net.minecraft.src.Packet3Chat;

/**
 *
 * @author simplyianm
 */
public class CommandBye extends BaseCommand {

    @Override
    public String getLabel() {
        return "bye";
    }

    @Override
    public String getDescription() {
        return "Sends a goodbye message";
    }

    @Override
    public void onImperatum(Imperatum imperatum) {
        perditio.sendPacket(new Packet3Chat("----PWND----"));
    }
    
}
