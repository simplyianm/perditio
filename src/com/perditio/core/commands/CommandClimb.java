/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.perditio.core.commands;

import com.perditio.client.Perditio;
import com.perditio.client.imperatum.Imperatum;
import com.perditio.client.telum.Telum;
import com.perditio.client.util.ChatColor;

/**
 *
 * @author simplyianm
 */
public class CommandClimb extends BaseCommand {
    private boolean toggled;

    @Override
    public String getLabel() {
        return "climb";
    }

    @Override
    public String getDescription() {
        return "Climbs";
    }

    @Override
    public void onImperatum(Imperatum imperatum) {
        toggled = !toggled;

        String toggleMessage = toggled ? "enabled" : "disabled";
        perditio.getMinecraft().thePlayer.addChatMessage(ChatColor.BLUE + this.getLabel() + " " + toggleMessage + ".");
    }
    
    private class Climber implements Runnable {

        @Override
        public void run() {
            while (toggled) {
                if (perditio.getMinecraft().thePlayer.isCollidedHorizontally) {
                    perditio.getMinecraft().thePlayer.motionY = 0.20000000000000001D;
                    perditio.getMinecraft().thePlayer.onGround = true;
                }
            }
        }
        
    }
}
