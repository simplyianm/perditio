/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.perditio.core.commands;

import com.perditio.client.Perditio;
import com.perditio.client.imperatum.Imperatum;
import com.perditio.client.telum.Telum;
import com.perditio.client.util.ChatColor;
import net.minecraft.src.Packet3Chat;

/**
 *
 * @author simplyianm
 */
public class CommandSpam extends BaseCommand {

    @Override
    public String getLabel() {
        return "spam";
    }

    @Override
    public String getDescription() {
        return "Spams a message.";
    }

    @Override
    public void onImperatum(Imperatum imperatum) {
        Spammer spammer = new Spammer(imperatum);
        Thread spamThread = new Thread(spammer, "TelumSpam");
        spamThread.start();
    }
    
    
    private class Spammer implements Runnable {
        private Imperatum imperatum;

        public Spammer(Imperatum imperatum) {
            this.imperatum = imperatum;
        }
        
        /**
         * Run
         */
        @Override
        public void run() {
            String message = "Narb";
            String amountStr = "100";
            
            if (imperatum.getArgs().length > 0) message = imperatum.getArgs()[0];
            if (imperatum.getArgs().length > 1) amountStr = imperatum.getArgs()[1];
            int amount;
            try {
                amount = Integer.parseInt(amountStr);
            } catch (NumberFormatException ex) {
                amount = 5;
            }
            for (int m = 0; m < amount; m++) {
                perditio.sendPacket(new Packet3Chat(message));
            }
        }
    }
}
