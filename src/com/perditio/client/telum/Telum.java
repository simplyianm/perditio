/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.perditio.client.telum;

import com.perditio.client.Perditio;
import com.perditio.client.imperatum.ImperatumExecutor;

/**
 *
 * @author simplyianm
 */
public abstract class Telum {
    protected Perditio perditio;
    
    private boolean initialized = false;
    private boolean enabled = false;
    
    /**
     * Initializes the Telum with the given Perditio instance.
     * 
     * @param perditio 
     */
    public final void initialize(Perditio perditio) {
        this.perditio = perditio;
        this.initialized = true;
    }
    
    /**
     * Enables the Telum.
     */
    public final void enable() {
        if (!initialized) System.out.println("uninitialized plugins cannot be enabled.");
        if (!enabled) {
            enabled = true;
            this.onEnable();
            System.out.println("[Perditio] " + this.getName() + " enabled.");
        }
    }

    /**
     * Disabled the Telum.
     */
    public final void disable() {
        if (enabled) {
            enabled = false;
            this.onDisable();
            System.out.println("[Perditio] " + this.getName() + " disabled.");
        }
    }
    
    /**
     * Updates the Telum.
     */
    public void update() {}

    public boolean isEnabled() {
        return enabled;
    }

    public boolean isInitialized() {
        return initialized;
    }
    
    public abstract void onEnable();
    
    public abstract void onDisable();
    
    public abstract String getName();
    
    public abstract String getDescription();
}
