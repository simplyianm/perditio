/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.perditio.client.telum;

/**
 * Represents a manager for {@link Telum}s.
 * @author simplyianm
 */
public interface TelumManager {
    /**
     * Registers a {@link Telum} with this {@link TelumManager}.
     * @param telum 
     */
    public void registerTelum(Telum telum);
    
    /**
     * Enables all {@link Telum}s associated with this {@link TelumManager}.
     */
    public void enableAll();
    
    /**
     * Disables all {@link Telum}s associated with this {@link TelumManager}.
     */
    public void disableAll();
    
    
    /**
     * Updates all {@link Telum}s associated with this {@link TelumManager}.
     */
    public void updateAll();
}
