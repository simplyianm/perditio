/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.perditio.client.telum;

import com.perditio.client.Perditio;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a Simple Telum Manager.
 * @author simplyianm
 */
public class SimpleTelumManager implements TelumManager {
    private Perditio perditio;
    private List<Telum> telums = new ArrayList<Telum>();
    
    public SimpleTelumManager(Perditio perditio) {
        this.perditio = perditio;
    }
    
    @Override
    public void registerTelum(Telum telum) {
        telums.add(telum);
    }

    @Override
    public void enableAll() {
        for (Telum telum : telums) {
            telum.initialize(perditio);
            telum.enable();
        }
    }

    @Override
    public void disableAll() {
        for (Telum telum : telums) {
            telum.disable();
        }
    }

    @Override
    public void updateAll() {
        for (Telum telum : telums) {
            telum.update();
        }
    }
    
}
