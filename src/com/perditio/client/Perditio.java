package com.perditio.client;

import com.perditio.client.imperatum.Imperatum;
import com.perditio.client.imperatum.ImperatumHandler;
import com.perditio.client.telum.SimpleTelumManager;
import com.perditio.client.telum.TelumManager;
import com.perditio.core.Core;

import net.minecraft.client.Minecraft;
import net.minecraft.src.GuiChat;
import net.minecraft.src.Packet;
import net.minecraft.src.Vec3D;
import org.lwjgl.input.Keyboard;

/**
 * main class
 */
public class Perditio {
    /**
     * Minecraft hook
     */
    private Minecraft minecraft;
    
    /**
     * Key bindings
     */
    public static int BINDING_COMMAND = Keyboard.KEY_TAB;
    
    /**
     * A boolean
     */
    public static boolean instant = false;
    
    private ImperatumHandler imperatumHandler;
    private TelumManager telumManager;
    
    /**
     * Constructor
     */
    public Perditio(Minecraft minecraft) {
        this.minecraft = minecraft;
        
        this.initialize();
    }
    
    private void initialize() {
        //Initialize the Imperatum handler
        imperatumHandler = new ImperatumHandler(this);
        
        //Initialize the Telum manager
        telumManager = new SimpleTelumManager(this);
        
        //Register built-in tela
        telumManager.registerTelum(new Core());
        
        //Enable all tela
        telumManager.enableAll();
    }
    
    public void update() {
        telumManager.updateAll();
        if (Keyboard.isKeyDown(BINDING_COMMAND)) this.promptCommand();
    }

    public Minecraft getMinecraft() {
        return minecraft;
    }

    public ImperatumHandler getImperatumHandler() {
        return imperatumHandler;
    }

    public TelumManager getTelumManager() {
        return telumManager;
    }
    
    /**
     * Parses the command given.
     * 
     * @param theCommand
     */
    public void parseTFCmd(String theCommand) {
        imperatumHandler.handleImperatum(new Imperatum(theCommand));
    }
    
    public void promptCommand() {
        minecraft.displayGuiScreen(new ILChat());
        ((ILChat) minecraft.currentScreen).setMessage("ILCMD>");
    }


        
    /**
     * Turns things into stairs.
     * 
     * @param args
     */
    public void tfCommandSteps(String args) {
        minecraft.thePlayer.stepHeight = 7;
        
    }

    /**
     * Help command
     * 
     * @param args
     */
    public void tfCommandHelp(String args) {
        minecraft.thePlayer.addChatMessage("�1TeamFascistClient �3Version Beta Commands:" +
        " " +
        "-------------------------------------------------------");
        minecraft.thePlayer.addChatMessage("detach - destroys everything instant hit near the player.");
        minecraft.thePlayer.addChatMessage("highjump - jump really high...");
        minecraft.thePlayer.addChatMessage("spam message=amount");
        minecraft.thePlayer.addChatMessage("dos =amount - lags certain servers.");
        minecraft.thePlayer.addChatMessage("climb - climb walls.");
        minecraft.thePlayer.addChatMessage("kill - force field!");
        minecraft.thePlayer.addChatMessage("panic - shuts down client.");
        minecraft.thePlayer.addChatMessage("sneak - make it appear you are sneaking...but you aren't");
    }
    
    
    /**
     * TP Command
     * 
     * @param args
     */
    private void tfCommandTp(String args) {
        Vec3D vec = minecraft.thePlayer.getLookVec();
        minecraft.thePlayer.setPosition(vec.xCoord, vec.yCoord, vec.zCoord);
    }

  
    public void sendPacket(Packet packet) {
        minecraft.getSendQueue().addToSendQueue(packet);
    }
    /*
    class TFRunnableSpeedhackListener implements Runnable {
        @Override
        public void run() { 
            while (true) {
                //while (tfToggleGet("speedhack") == true) {
                    if (Keyboard.isKeyDown(Keyboard.KEY_LSHIFT) && tfToggleGet("fly") == false) {
                        minecraft.thePlayer.motionX *= 4D;
                        minecraft.thePlayer.motionY *= 4D;
                        minecraft.thePlayer.motionZ *= 4D;
                    }
               // }
            }
        }
    }*/

    public class ILChat extends GuiChat {
        public ILChat() {
        }

        public void setMessage(String s) {
            message = s;
        }

        @Override
        public void keyTyped(char c, int i) {
            if(i == 28) {
                String s = message.trim();
                if(s.startsWith("ILCMD>")) {
                    parseTFCmd(s.substring(6));
                    message = "";
                }
            }
            super.keyTyped(c, i);
        }

        public String getName() {
            return "ILChat";
        }

        public void uiCommand(String s) {
        }
    }
    
    
}
