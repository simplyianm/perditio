/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.perditio.client.imperatum;

/**
 * Executes Imperata.
 * @author simplyianm
 */
public interface ImperatumExecutor {
    /**
     * Called when an {@link Imperatum} is sent.
     * @param imperatum 
     */
    public void onImperatum(Imperatum imperatum);
}