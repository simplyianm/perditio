/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.perditio.client.imperatum;

/**
 * Represents an Imperatum, aka an order.
 * @author simplyianm
 */
public final class Imperatum {
    private String label;
    private String[] args;
    
    public Imperatum(String commandString) {
        this.processImperataString(commandString);
    }
    
    /**
     * Processes the String which the command originated from.
     * 
     * @param commandString 
     */
    public final void processImperataString(String commandString) {
        String[] split = commandString.split(" ");
        this.label = split[0];
        if (split.length > 1) {
            args = new String[split.length - 1];
            //Shift arg indices by -1
            for (int i = 0; i < split.length - 1; i++) {
                args[i] = split[i + 1];
            }
        } else {
            args = new String[0];
        }
    }

    public String[] getArgs() {
        return args;
    }

    /**
     * Gets the name for this {@link Imperatum}.
     * 
     * @return The command name
     */
    public String getLabel() {
        return label;
    }
    
    /**
     * Returns all args as a string separated by spaces.
     * 
     * @return 
     */
    public String getArgString() {
        String spaceyArgs = "";
        for (int i = 0; i < args.length; i++) {
            spaceyArgs += args[i];
            if (i != args.length - 1) spaceyArgs += " ";
        }
        return spaceyArgs;
    }
    
    @Override
    public String toString() {
        return label + " " + getArgString();
    }
}
