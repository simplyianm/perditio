/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.perditio.client.imperatum;

import com.perditio.client.Perditio;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * Handles Imperata and sends them to executors.
 * 
 * @author simplyianm
 */
public class ImperatumHandler {
    private HashMap<String, List<ImperatumExecutor>> registeredExecutors = new HashMap<String, List<ImperatumExecutor>>();
    private Perditio perditio;
    
    public ImperatumHandler(Perditio perditio) {
        this.perditio = perditio;
    }
    
    /**
     * Handles a Imperatum for the server.
     * 
     * @param Imperatum 
     */
    public void handleImperatum(Imperatum imperatum) {
        List<ImperatumExecutor> imperatumExecutors = registeredExecutors.get(imperatum.getLabel().toLowerCase());
        perditio.getMinecraft().thePlayer.addChatMessage("Regged: " + registeredExecutors.toString());
        if (imperatumExecutors == null) return;
        
        //Handle the Imperatum
        Iterator<ImperatumExecutor> it = imperatumExecutors.iterator();
        while (it.hasNext()) {
            it.next().onImperatum(imperatum);
        }
        
        perditio.getMinecraft().thePlayer.addChatMessage("Imp: " + imperatumExecutors.toString());
        
        
    }
    
    /**
     * Registers an {@link ImperatumExecutor} for this {@link ImperatumHandler}.
     * @param executor 
     */
    public void registerExecutor(String imperatumLabel, ImperatumExecutor executor) {
        List<ImperatumExecutor> commandExecutors = registeredExecutors.get(imperatumLabel.toLowerCase());
        if (commandExecutors == null) {
            commandExecutors = new ArrayList<ImperatumExecutor>();
            registeredExecutors.put(imperatumLabel.toLowerCase(), commandExecutors);
        }
        commandExecutors.add(executor);
    }
}
